package com.virtualcompiler.mapandlocationtest;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mcc.codewarrior.adapter.ContactListAdapter;
import com.mcc.codewarrior.entity.Person;
import com.mcc.codewarrior.entity.PhoneUtils;
import com.mcc.codewarrior.indexablelistview.IndexableListView;

public class ContactListActivity extends Activity {

	private ListView contactListView;
	public static ArrayList<Person> persons;

	ContactListAdapter contactListAdapter;
	private Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contactlist);

		contactListView = (IndexableListView) findViewById(R.id.contactListView);

		persons = new PhoneUtils().getAllContactsWithPhoto(context);

		contactListAdapter = new ContactListAdapter(context,
				R.layout.allcontact_list_row, R.id.contactNameTextView, persons);
		contactListView.setAdapter(contactListAdapter);
		contactListView.setFastScrollEnabled(true);
		contactListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent();
//				intent.putExtra("position", position);
//				intent.setClass(context, ContactDetailsActivity.class);
//				startActivity(intent);
			}

		});

	}
}
