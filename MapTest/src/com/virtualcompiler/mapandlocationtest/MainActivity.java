package com.virtualcompiler.mapandlocationtest;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends Activity {

	GoogleMap map;
	TextView textView_Address;

	Button btnSelectContact;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		textView_Address = (TextView) findViewById(R.id.textView_Address);
		btnSelectContact = (Button) findViewById(R.id.btnSelectContact);

		btnSelectContact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Intent newActivity = new Intent(MainActivity.this,
						ContactListActivity.class);
				startActivity(newActivity);

			}
		});

		setMapIfNeeded();
		// getMyLocationAddress();

	}

	// set the map
	private void setMapIfNeeded() {

		if (map == null) {

			map = ((MapFragment) getFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			if (map != null) {

				setUpMap();

			}
		}

	}

	private void setUpMap() {

		// set a icon on map to go your current position
		map.setMyLocationEnabled(true);

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		Criteria criteria = new Criteria();

		String provider = locationManager.getBestProvider(criteria, true);

		Location myLocation = locationManager.getLastKnownLocation(provider);

		// set map type
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

		// get lat , long from best provider
		double latitude = myLocation.getLatitude();
		double longitude = myLocation.getLongitude();

		// create an object with above two
		LatLng current = new LatLng(latitude, longitude);

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 13));

		// set map zoom level
		map.animateCamera(CameraUpdateFactory.zoomTo(13));

		// add a maker to your position.
		map.addMarker(new MarkerOptions().title("Your are here")
				.snippet("The most populous city in Australia.")
				.position(current));

		getMyLocationAddress(latitude, longitude);
	}

	// Reverse Geocoding: get address name from latitude & longitude.
	public String getMyLocationAddress(double lat, double longi) {

		Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
		String address = "";

		try {

			// Place your latitude and longitude
			List<Address> addresses = geocoder.getFromLocation(lat, longi, 1);

			if (addresses != null) {

				Address fetchedAddress = addresses.get(0);
				StringBuilder strAddress = new StringBuilder();

				for (int i = 0; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
					strAddress.append(fetchedAddress.getAddressLine(i)).append(
							"\n");
				}

				textView_Address.setText("I am at: " + strAddress.toString());

				address = strAddress.toString();

			}

			else
				textView_Address.setText("No location found..!");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Could not get address..!",
					Toast.LENGTH_LONG).show();
		}

		return address;
	}

	// code from devs library
	// not used here
	private double[] getGPS() {

		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		List<String> providers = lm.getProviders(true);

		/*
		 * Loop over the array backwards, and if you get an accurate location,
		 * then break out the loop
		 */
		Location l = null;

		for (int i = providers.size() - 1; i >= 0; i--) {
			l = lm.getLastKnownLocation(providers.get(i));
			if (l != null)
				break;
		}

		double[] gps = new double[2];
		if (l != null) {
			gps[0] = l.getLatitude();
			gps[1] = l.getLongitude();
		}
		return gps;
	}
}
