package com.mcc.codewarrior.entity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;

public class PhoneUtils {

	private static final String TAG = "PhoneUtils";

	public static Person getselectedNumberDetails() {

		return null;
	}

	public ArrayList<Person> getAllContactsWithPhoto(Context context) {

		ArrayList<Person> allContacts = new ArrayList<Person>();
		ContentResolver cr = context.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));

//				if (Integer
//						.parseInt(cur.getString(cur
//								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

					Cursor pCur = cr.query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null, ContactsContract.Data.CONTACT_ID + " = ?",
							new String[] { id }, null);

					while (pCur.moveToNext()) {

						// fill object data
						Person aPerson = new Person();
						aPerson.setName(pCur.getString(pCur
								.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)));
						aPerson.setNumber(pCur.getString(pCur
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));

						// aPerson.setImage_Uri(getPhotoUri(context, id));
						String test = null;
						Uri my_contact_Uri = null;
						test = pCur
								.getString(pCur
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
						if (test != null) {
							my_contact_Uri = Uri.parse(test);
						}

						try {
							if (my_contact_Uri != null) {
								Bitmap bitmap = MediaStore.Images.Media
										.getBitmap(
												context.getContentResolver(),
												my_contact_Uri);
								aPerson.setImage_in_bitmap(bitmap);
							} else {
								// no need to set
							}

						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						Log.d(" Number List",
								" " + aPerson.getName() + " "
										+ aPerson.getNumber() + " "
										+ aPerson.getImage_Uri());

						allContacts.add(aPerson);
					}
					pCur.close();
//				}
			}
		}

		return allContacts;
	}

	public ArrayList<Person> getAllContactsWithoutPhoto(Context context) {

		ArrayList<Person> allContacts = new ArrayList<Person>();
		ContentResolver cr = context.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));

				String ur_string = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));

				if (Integer
						.parseInt(cur.getString(cur
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					Cursor pCur = cr.query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { id }, null);

					while (pCur.moveToNext()) {

						Person aBlockNumber = new Person();

						aBlockNumber
								.setName(pCur.getString(pCur
										.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)));
						aBlockNumber
								.setNumber(pCur.getString(pCur
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));

						// Log.d(" Number List", " " + aBlockNumber.getName()
						// + " " + aBlockNumber.getNumber() + " "
						// + aBlockNumber.getImage_uri());

						allContacts.add(aBlockNumber);
					}
					pCur.close();
				}
			}
		}
		return allContacts;
	}

	public static Uri getPhotoUri(Context context, String id) {

		Uri person = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
		Uri photo = Uri.withAppendedPath(person,
				ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
		Cursor cur = context
				.getContentResolver()
				.query(ContactsContract.Data.CONTENT_URI,
						null,
						ContactsContract.Data.CONTACT_ID
								+ "="
								+ id
								+ " AND "
								+ ContactsContract.Data.MIMETYPE
								+ "='"
								+ ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
								+ "'", null, null);
		if (cur != null) {
			if (!cur.moveToFirst()) {
				return null; // no photo
			}
		} else {
			return null; // error in cursor process
		}
		cur.close();
		return photo;
	}
}