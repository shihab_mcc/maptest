package com.mcc.codewarrior.entity;

import android.graphics.Bitmap;
import android.net.Uri;

public class Person {

	private String name;
	private String phone_number;
	private String lat;
	private String lon;
	private Uri image_uri;
	private Bitmap image_in_bitmap=null;
	private boolean selected = false;

	
	
	public Person(String name, String phone_number, String lat, String lon,
			Uri image_uri, Bitmap image_in_bitmap) {
		super();
		this.name = name;
		this.phone_number = phone_number;
		this.lat = lat;
		this.lon = lon;
		this.image_uri = image_uri;
		this.image_in_bitmap = image_in_bitmap;
	}

	public Person() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return phone_number;
	}

	public void setNumber(String number) {
		this.phone_number = number;
	}

	public Uri getImage_Uri() {
		return image_uri;
	}

	public void setImage_Uri(Uri image_uri) {
		this.image_uri = image_uri;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Bitmap getImage_in_bitmap() {
		return image_in_bitmap;
	}

	public void setImage_in_bitmap(Bitmap image_in_bitmap) {
		this.image_in_bitmap = image_in_bitmap;
	}

}
