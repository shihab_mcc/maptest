package com.mcc.codewarrior.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.mcc.codewarrior.entity.Person;
import com.virtualcompiler.mapandlocationtest.R;

public class ContactListAdapter extends BaseAdapter implements SectionIndexer, Filterable{

	private ArrayList<Person> allContactsNumbers = null;
	private ArrayList<Person> holdallContactsNumbers;
	public Context context;
	public LayoutInflater inflater = null;
	private ViewHolder holder;
	private Bitmap bitmap;
	private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private Valuefilter valuefilter;
	
	public ContactListAdapter(Context context, int resource, int resourceId, ArrayList<Person> allNumberList ) {
		
		this.context = context;
		this.allContactsNumbers = allNumberList;
		this.holdallContactsNumbers = allNumberList;
		this.inflater = (LayoutInflater) this.context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			holder = new ViewHolder();

			convertView = inflater.inflate(R.layout.allcontact_list_row, null);
			convertView.setMinimumHeight(50);

			holder.textViewContactName = (TextView) convertView.findViewById(R.id.contactNameTextView);
			holder.textView_Contact_Number = (TextView) convertView.findViewById(R.id.numberTextView);
			holder.imgViewContactImage = (ImageView) convertView.findViewById(R.id.imgViewContactImage);


			// holder.checkBox.setOnClickListener(new View.OnClickListener() {
			// public void onClick(View v) {
			//
			// OnClickPersonContact((Person) v.getTag(), 1);
			// // CheckBox cb = (CheckBox) v;
			// // if (cb.isChecked()) {
			// // OnClickPersonContact((Person) cb.getTag(), 1);
			// // }else
			// // OnClickPersonContact((Person) cb.getTag(), 2);
			//
			// }
			// });
//			holder.checkBox
//					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//						@Override
//						public void onCheckedChanged(CompoundButton view,
//								boolean isChecked) {
//							int getPosition = (Integer) view.getTag();
//							allContactsNumbers.get(getPosition).setSelected(
//									view.isChecked());
//							if (isChecked) {
//
//								OnClickPersonContact(
//										allContactsNumbers.get(getPosition), 1);
//							} else {
//								OnClickPersonContact(
//										allContactsNumbers.get(getPosition), 2);
//							}
//
//						}
//					});
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		if (allContactsNumbers.size() <= 0) {
			holder.textViewContactName.setText("No Data");

		} else {
			holder = (ViewHolder) convertView.getTag();

			String name = allContactsNumbers.get(position).getName();
			holder.textViewContactName.setText(name);

			String number = allContactsNumbers.get(position).getNumber();
			holder.textView_Contact_Number.setText(number);

			
			Bitmap aBitmap=allContactsNumbers.get(position).getImage_in_bitmap();
			if (aBitmap!=null) {
				holder.imgViewContactImage.setImageBitmap(aBitmap);
			} else {
				holder.imgViewContactImage.setImageResource(R.drawable.ic_no_image);
			}

		}
		return convertView;
	}

	public static class ViewHolder {
		TextView textViewContactName;
		TextView textView_Contact_Number;
		ImageView imgViewContactImage;
		CheckBox checkBox;
	}

//	@Override
//	public int getPositionForSection(int section) {
//		// If there is no item for current section, previous section will be
//		// selected
//		// char t = g.getGeneric_name().toString();
//		for (int i = section; i >= 0; i--) {
//			for (int j = 0; j < getCount(); j++) {
//				if (i == 0) {
//					// For numeric section
//					for (int k = 0; k <= 9; k++) {
//						if (StringMatcher.match(String.valueOf((allContactsNumbers.get(j).getName()).charAt(0)), String.valueOf(k)))
//							return j;
//					}
//				} else {
//					if (StringMatcher
//							.match(String.valueOf((allContactsNumbers.get(j).getName()).charAt(0)), String.valueOf(mSections.charAt(i))))
//						return j;
//				}
//			}
//		}
//		return 0;
//	}

	@Override
	public int getSectionForPosition(int position) {
		return 0;
	}

	@Override
	public Object[] getSections() {
		String[] sections = new String[mSections.length()];
		for (int i = 0; i < mSections.length(); i++)
			sections[i] = String.valueOf(mSections.charAt(i));
		return sections;
	}
	
	
	public class Valuefilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			FilterResults results = new FilterResults();
			allContactsNumbers = holdallContactsNumbers;
			if (constraint == null || constraint.length() == 0) {
				results.values = allContactsNumbers;
				results.count = allContactsNumbers.size();
			} else {

				ArrayList<Person> newCategoriesList = new ArrayList<Person>();

				for (Person person : allContactsNumbers) {
					if (person.getName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
						newCategoriesList.add(person);
				}

				results.values = newCategoriesList;
				results.count = newCategoriesList.size();

			}

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			if (results.count == 0)
				notifyDataSetInvalidated();
			else {
				allContactsNumbers = (ArrayList<Person>) results.values;
				notifyDataSetChanged();
			}

		}

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return allContactsNumbers.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Filter getFilter() {
		if (valuefilter == null)
			valuefilter = new Valuefilter();

		return valuefilter;
	}

	@Override
	public int getPositionForSection(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
